/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.924535237037279, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.48096532970768185, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999291383219955, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.8904203997243281, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7339974565493853, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.7913804945054945, 500, 1500, "me"], "isController": false}, {"data": [0.9550330639235856, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.793163585629578, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.8994035329203945, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.005952380952380952, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 61214, 0, 0.0, 244.2506289410912, 11, 13848, 45.0, 645.0, 1056.9000000000015, 1954.0, 202.70408991115514, 671.4340210112521, 246.23266714502608], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 1471, 0, 0.0, 1017.9707681849073, 183, 2230, 966.0, 1431.0, 1552.7999999999997, 1937.1199999999992, 4.897130301617951, 3.0941829151824356, 6.0592422774901795], "isController": false}, {"data": ["getLatestMobileVersion", 35280, 0, 0.0, 42.09056122449012, 11, 1364, 25.0, 80.0, 137.0, 215.0, 117.63764404609475, 78.69315055817862, 86.73478638164212], "isController": false}, {"data": ["findAllConfigByCategory", 4353, 0, 0.0, 343.2910636342748, 30, 1780, 229.0, 766.1999999999998, 994.3000000000002, 1310.0, 14.510048366827888, 16.408824227330758, 18.84605891394638], "isController": false}, {"data": ["getNotifications", 2359, 0, 0.0, 633.9516744383219, 62, 2029, 489.0, 1347.0, 1476.0, 1702.2000000000007, 7.862782938527636, 66.73383449096063, 8.745810319319315], "isController": false}, {"data": ["getHomefeed", 136, 0, 0.0, 11051.654411764703, 4909, 13848, 11135.5, 12372.6, 12868.25, 13758.46, 0.450359459701107, 7.876452699010865, 2.2535565151449926], "isController": false}, {"data": ["me", 2912, 0, 0.0, 513.504807692308, 56, 1924, 386.0, 1177.0, 1315.35, 1596.4399999999987, 9.70614900538638, 12.769345009932803, 30.7582553930457], "isController": false}, {"data": ["getAllClassInfo", 6805, 0, 0.0, 219.47421013960357, 18, 1602, 157.0, 474.0, 656.3999999999996, 1016.8799999999992, 22.685904402498952, 13.115288482694705, 58.24340104899388], "isController": false}, {"data": ["findAllChildrenByParent", 2867, 0, 0.0, 521.6306243460053, 58, 1877, 380.0, 1218.4000000000005, 1356.1999999999998, 1600.3200000000002, 9.555711095557111, 10.73151148426824, 15.266741555011167], "isController": false}, {"data": ["findAllSchoolConfig", 4359, 0, 0.0, 342.9499885294798, 42, 1755, 237.0, 730.0, 987.0, 1298.7999999999993, 14.530726536326817, 316.89471192309617, 10.656812137481873], "isController": false}, {"data": ["getChildCheckInCheckOut", 672, 0, 0.0, 2227.7127976190486, 1340, 3438, 2206.0, 2793.4, 2906.7, 3186.4499999999994, 2.2366748101156277, 149.32207035540495, 10.292198930922694], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 61214, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
